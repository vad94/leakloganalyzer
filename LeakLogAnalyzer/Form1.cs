﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace LeakLogAnalyzer
{
    public partial class Form1 : Form
    {
        public class LeakInfo
        {
            public Int32 id, address, count, size;
            public string stack, data;
        }

        public List<LeakInfo> leak_list;
        public Dictionary<Int32, List<LeakInfo>> leak_groups;

        class BlockItemComparer : IComparer
        {
            private SortOrder direction;
            private int column;

            public BlockItemComparer(int _column, SortOrder _direction)
            {
                column = _column;
                direction = _direction;
            }
            public int Compare(object x, object y)
            {
                int returnVal = -1;

                Int32 a = Convert.ToInt32(((ListViewItem)x).SubItems[column].Text, column == 1 ? 16 : 10);
                Int32 b = Convert.ToInt32(((ListViewItem)y).SubItems[column].Text, column == 1 ? 16 : 10);

                if (a < b)
                    return direction == SortOrder.Ascending ? 1 : -1;

                if (a > b)
                    return direction == SortOrder.Ascending ? -1 : 1;
                else
                    return 0;
            }
        }

        class GroupItemComparer : IComparer
        {
            private SortOrder direction;
            private int column;

            public GroupItemComparer(int _column, SortOrder _direction)
            {
                column = _column;
                direction = _direction;
            }
            public int Compare(object x, object y)
            {

                Int32 a = Convert.ToInt32(((ListViewItem)x).SubItems[column].Text);
                Int32 b = Convert.ToInt32(((ListViewItem)y).SubItems[column].Text);

                if (a < b)
                    return direction == SortOrder.Ascending ? 1 : -1;

                if (a > b)
                    return direction == SortOrder.Ascending ? -1 : 1;
                else
                    return 0;
            }
        }

        public Form1()
        {
            InitializeComponent();
            GroupDepthComboBox.SelectedIndex = 0;
            leak_list = new List<LeakInfo>();
            leak_groups = new Dictionary<int, List<LeakInfo>>();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                leak_list.Clear();

                UInt64 total = 0;

                string file_str = File.ReadAllText(openFileDialog.FileName);
                file_str = file_str.Replace("\r", "");
                //string pattern = @"(?<block>---------- Block (?>.+\n)+(\n){2})";
                string pattern = @"(?<block>---------- Block (?<id>\d+) at (?<addr>.+?): (?<size>\d+).+\n.+Count: (?<count>\d+).+\n.+(?<stack>(?>\n|.)+?)  Data:\n(?<data>(?>\n|.)+?)\n\n)";

                foreach (Match match in Regex.Matches(file_str, pattern, RegexOptions.IgnoreCase))
                {
                    try
                    {
                        string block = match.Groups["block"].Value;//file_str.Substring(match.Groups["block"].Index, match.Groups["block"].Length);

                        LeakInfo leak = new LeakInfo();
                        leak.id = Convert.ToInt32(match.Groups["id"].Value);
                        leak.size = Convert.ToInt32(match.Groups["size"].Value);
                        leak.count = Convert.ToInt32(match.Groups["count"].Value);
                        leak.address = Convert.ToInt32(match.Groups["addr"].Value, 16);
                        leak.data = match.Groups["data"].Value;
                        leak.stack = match.Groups["stack"].Value;
                        leak_list.Add(leak);
                        total += (UInt64) (leak.size * leak.count);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        continue;
                    }

                    labelTotal.Text = "Total: " + (total / 1024 / 1024).ToString() + " MB";

                    //string[] row = {
                    //                   match.Groups["id"].Value,
                    //                   match.Groups["addr"].Value,
                    //                   match.Groups["count"].Value,
                    //                   match.Groups["size"].Value,
                    //                   Convert.ToString(leak.size * leak.count)
                    //               };

                    //ListViewItem item = new ListViewItem(row);
                    //BlockListView.Items.Add(item);

                    //item.Tag = match.Groups["stack"].Value;
                }
            }
            UpdateGroups();
        }

        public static string DeleteLines(string s, int linesToRemove)
        {
            return s.Split(Environment.NewLine.ToCharArray(),
                           linesToRemove + 1
                ).Skip(linesToRemove)
                .FirstOrDefault();
        }

        bool IsSameGroup(LeakInfo a, LeakInfo b, int stack_depth)
        {
            int lines_a = a.stack.Split('\n').Length;
            int lines_b = b.stack.Split('\n').Length;

            if (Math.Abs(lines_a - lines_b) > stack_depth)
                return false;

            int compare_lines = Math.Max(lines_a - stack_depth, lines_b - stack_depth);

            string stack_a = DeleteLines(a.stack, Math.Abs(lines_a - compare_lines));
            string stack_b = DeleteLines(b.stack, Math.Abs(lines_b - compare_lines));

            if (String.Equals(stack_a, stack_b))
                return true;

            return false;
        }

        void UpdateGroups()
        {
            if (leak_list == null)
                return;

            //update structs
            {
                leak_groups.Clear();
                List<LeakInfo> leaks = leak_list.ToList();

                int group = 0;

                while (leaks.Count != 0)
                {
                    LeakInfo first_group_leak =  leaks[0];
                    leaks.RemoveAt(0);

                    List<LeakInfo> group_leaks = new List<LeakInfo>();
                    group_leaks.Add(first_group_leak);

                    for (int i = leaks.Count - 1; i >=0; i--)
                    {
                        if (IsSameGroup(first_group_leak, leaks[i], Convert.ToInt32(GroupDepthComboBox.Text)))
                        {
                            group_leaks.Add(leaks[i]);
                            leaks.RemoveAt(i);
                        }
                    }

                    leak_groups.Add(group++, group_leaks);
                }
            }

            //update lists
            GroupListView.Items.Clear();
            BlockListView.Items.Clear();
            foreach(KeyValuePair<Int32, List<LeakInfo>> group in leak_groups)
            {
                int total_count = 0;
                int total_size = 0;

                foreach (LeakInfo leak in group.Value)
                {
                    total_count += leak.count;
                    total_size += (leak.size * leak.count);
                }

                string[] row = {
                                   group.Key.ToString(),
                                   group.Value.Count.ToString(),
                                   total_count.ToString(),
                                   total_size.ToString(),
                                   (total_size/1024/1024).ToString()
                               };

                ListViewItem item = new ListViewItem(row);
                GroupListView.Items.Add(item);
            }
        }

        private void BlockListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BlockListView.SelectedItems.Count == 1)
            {
                StackTextBox.Clear();
                StackTextBox.AppendText(BlockListView.SelectedItems[0].Tag.ToString());
            }
        }

        private void BlockListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (BlockListView.Sorting != SortOrder.Ascending)
                BlockListView.Sorting = SortOrder.Ascending;
            else
                BlockListView.Sorting = SortOrder.Descending;

            BlockListView.ListViewItemSorter = new BlockItemComparer(e.Column, BlockListView.Sorting);
            BlockListView.Sort();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateGroups();
        }

        private void GroupListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (GroupListView.Sorting != SortOrder.Ascending)
                GroupListView.Sorting = SortOrder.Ascending;
            else
                GroupListView.Sorting = SortOrder.Descending;

            GroupListView.ListViewItemSorter = new GroupItemComparer(e.Column, GroupListView.Sorting);
            GroupListView.Sort();
        }

        private void GroupListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GroupListView.SelectedItems.Count == 1)
            {
                BlockListView.Items.Clear();



                List<LeakInfo> leaks = leak_groups[Convert.ToInt32(GroupListView.SelectedItems[0].SubItems[0].Text)];

                foreach (LeakInfo leak in leaks)
                {
                    string[] row = {
                                   leak.id.ToString(),
                                   "0x" + leak.address.ToString("X8"),
                                   leak.count.ToString(),
                                   leak.size.ToString(),
                                   Convert.ToString(leak.size * leak.count)
                               };

                    ListViewItem item = new ListViewItem(row);
                    BlockListView.Items.Add(item);

                    item.Tag = leak.stack;
                }


            }
        }
    }
}
