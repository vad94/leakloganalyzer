﻿namespace LeakLogAnalyzer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.GroupListView = new System.Windows.Forms.ListView();
            this.columnGroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBlocks = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTotalCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTotalSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTotalSizeMB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupDepthComboBox = new System.Windows.Forms.ComboBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.BlockListView = new System.Windows.Forms.ListView();
            this.columnBlock = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAddr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSumSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StackTextBox = new System.Windows.Forms.RichTextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.labelTotal = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            //
            // menuStrip1
            //
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1291, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            //
            // fileToolStripMenuItem
            //
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            //
            // openToolStripMenuItem
            //
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            //
            // splitContainer1
            //
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            //
            // splitContainer1.Panel1
            //
            this.splitContainer1.Panel1.Controls.Add(this.GroupListView);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            //
            // splitContainer1.Panel2
            //
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1291, 696);
            this.splitContainer1.SplitterDistance = 555;
            this.splitContainer1.TabIndex = 4;
            //
            // GroupListView
            //
            this.GroupListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnGroup,
            this.columnBlocks,
            this.columnTotalCount,
            this.columnTotalSize,
            this.columnTotalSizeMB});
            this.GroupListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupListView.FullRowSelect = true;
            this.GroupListView.HideSelection = false;
            this.GroupListView.Location = new System.Drawing.Point(0, 0);
            this.GroupListView.Name = "GroupListView";
            this.GroupListView.Size = new System.Drawing.Size(555, 654);
            this.GroupListView.TabIndex = 1;
            this.GroupListView.UseCompatibleStateImageBehavior = false;
            this.GroupListView.View = System.Windows.Forms.View.Details;
            this.GroupListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.GroupListView_ColumnClick);
            this.GroupListView.SelectedIndexChanged += new System.EventHandler(this.GroupListView_SelectedIndexChanged);
            //
            // columnGroup
            //
            this.columnGroup.Text = "Group";
            this.columnGroup.Width = 100;
            //
            // columnBlocks
            //
            this.columnBlocks.Text = "Blocks";
            this.columnBlocks.Width = 100;
            //
            // columnTotalCount
            //
            this.columnTotalCount.Text = "Total Count";
            this.columnTotalCount.Width = 100;
            //
            // columnTotalSize
            //
            this.columnTotalSize.Text = "Total Size";
            this.columnTotalSize.Width = 100;
            //
            // columnTotalSizeMB
            //
            this.columnTotalSizeMB.Text = "Total Size (MB)";
            this.columnTotalSizeMB.Width = 100;
            //
            // panel1
            //
            this.panel1.Controls.Add(this.labelTotal);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.GroupDepthComboBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 654);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(555, 42);
            this.panel1.TabIndex = 0;
            //
            // label1
            //
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Call Stack idenity depth:";
            //
            // GroupDepthComboBox
            //
            this.GroupDepthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GroupDepthComboBox.FormattingEnabled = true;
            this.GroupDepthComboBox.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25"});
            this.GroupDepthComboBox.Location = new System.Drawing.Point(164, 6);
            this.GroupDepthComboBox.Name = "GroupDepthComboBox";
            this.GroupDepthComboBox.Size = new System.Drawing.Size(153, 21);
            this.GroupDepthComboBox.TabIndex = 0;
            this.GroupDepthComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            //
            // splitContainer2
            //
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            //
            // splitContainer2.Panel1
            //
            this.splitContainer2.Panel1.Controls.Add(this.BlockListView);
            //
            // splitContainer2.Panel2
            //
            this.splitContainer2.Panel2.Controls.Add(this.StackTextBox);
            this.splitContainer2.Size = new System.Drawing.Size(732, 696);
            this.splitContainer2.SplitterDistance = 318;
            this.splitContainer2.TabIndex = 0;
            //
            // BlockListView
            //
            this.BlockListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnBlock,
            this.columnAddr,
            this.columnCount,
            this.columnSize,
            this.columnSumSize});
            this.BlockListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BlockListView.FullRowSelect = true;
            this.BlockListView.HideSelection = false;
            this.BlockListView.Location = new System.Drawing.Point(0, 0);
            this.BlockListView.Name = "BlockListView";
            this.BlockListView.Size = new System.Drawing.Size(732, 318);
            this.BlockListView.TabIndex = 1;
            this.BlockListView.UseCompatibleStateImageBehavior = false;
            this.BlockListView.View = System.Windows.Forms.View.Details;
            this.BlockListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.BlockListView_ColumnClick);
            this.BlockListView.SelectedIndexChanged += new System.EventHandler(this.BlockListView_SelectedIndexChanged);
            //
            // columnBlock
            //
            this.columnBlock.Text = "Block";
            this.columnBlock.Width = 100;
            //
            // columnAddr
            //
            this.columnAddr.Text = "Address";
            this.columnAddr.Width = 100;
            //
            // columnCount
            //
            this.columnCount.Text = "Count";
            this.columnCount.Width = 100;
            //
            // columnSize
            //
            this.columnSize.Text = "Size";
            this.columnSize.Width = 100;
            //
            // columnSumSize
            //
            this.columnSumSize.Text = "Sum Size";
            this.columnSumSize.Width = 100;
            //
            // StackTextBox
            //
            this.StackTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StackTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StackTextBox.Location = new System.Drawing.Point(0, 0);
            this.StackTextBox.Name = "StackTextBox";
            this.StackTextBox.Size = new System.Drawing.Size(732, 374);
            this.StackTextBox.TabIndex = 0;
            this.StackTextBox.Text = "";
            //
            // openFileDialog
            //
            this.openFileDialog.Filter = "txt files (*.txt)|*.txt";
            //
            // labelTotal
            //
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(323, 14);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(34, 13);
            this.labelTotal.TabIndex = 2;
            this.labelTotal.Text = "Total:";
            //
            // Form1
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1291, 720);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Visual Leak Detector Log Alalyzer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView BlockListView;
        private System.Windows.Forms.RichTextBox StackTextBox;
        private System.Windows.Forms.ColumnHeader columnBlock;
        private System.Windows.Forms.ColumnHeader columnAddr;
        private System.Windows.Forms.ColumnHeader columnCount;
        private System.Windows.Forms.ColumnHeader columnSize;
        private System.Windows.Forms.ColumnHeader columnSumSize;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ListView GroupListView;
        private System.Windows.Forms.ColumnHeader columnGroup;
        private System.Windows.Forms.ColumnHeader columnTotalCount;
        private System.Windows.Forms.ColumnHeader columnTotalSize;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox GroupDepthComboBox;
        private System.Windows.Forms.ColumnHeader columnBlocks;
        private System.Windows.Forms.ColumnHeader columnTotalSizeMB;
        private System.Windows.Forms.Label labelTotal;
    }
}

